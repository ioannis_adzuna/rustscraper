pub struct History {
    list: Vec<String>,
}

impl History {

    pub fn add(&mut self, url: &str) {
        self.list.push(url);
    }

}
