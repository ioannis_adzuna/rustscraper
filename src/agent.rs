extern crate reqwest;

use ::agent::history;


pub struct Agent {
    history: History,
}


impl Agent {



    pub fn visit(&mut self, url: &str) -> String {
        self.history.add(url);
        return self.get(url);
    }



    pub fn get(&mut self, url: &str) -> String {

        // default result
        let empty_string = String::new();

        let html_text = match reqwest::get(url) {

            Err(_ignored) => empty_string,

            Ok(mut response) => match response.text() {

                Err(_ignored) => empty_string,

                Ok(text) => text,

            },
        };

        return html_text;
    }


}





enum Method {
    GET,
    POST,
    HEAD,
    PUT,
    DELETE,
    OPTIONS,
    TRACE,
    CONNECT,
    PATCH,
}



enum ResponseType {
    R100,   // informational
    R200,   // success
    R300,   // redirection
    R400,   // client error
    R500,   // server error
}
